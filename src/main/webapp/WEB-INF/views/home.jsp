<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<p> Jmeno uzivatele 1: <strong>${user.username}</strong>. </p>

<div>
	<table border="1">
	<tr>
		<th>id</th>
		<th>Jmeno</th>
		<th>JSON</th>
	</tr>
	<c:forEach var="user" items="${vsechnyUzivatele}">
	<tr>
		<td><c:out value="${user.id}" /></td>
		<td><c:out value="${user.username}" /></td>
		<td>
			<a href='<c:url value="/user/getById/${user.id}" />'>JSON</a>
		</td>
	</tr>
	</c:forEach>	
	</table>
	<a href='<c:url value="/user/all" />'>JSON vsech uzivatelu</a>
	
</div>

</body>
</html>
