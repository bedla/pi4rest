package eu.janbednar.pi4rest.utils;

import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinState;

public class GpioUtils {
	public static PinState booleanToPinState(boolean isHigh){
		PinState pinState = PinState.LOW;
		if (isHigh) pinState = PinState.HIGH;
		return pinState;
	}
	
public static boolean pinStateToBoolean(PinState pinState)
	{
		return pinState == PinState.HIGH;
	}

public static PinMode booleanToPinMode(boolean isInput){
		PinMode pinMode =PinMode.DIGITAL_OUTPUT;
		if (isInput) pinMode = PinMode.DIGITAL_INPUT;
		return pinMode;
	}

public static boolean pinModeToBoolean(PinMode pinMode)
	{
		return pinMode == PinMode.DIGITAL_INPUT;
	}
}
