package eu.janbednar.pi4rest.utils;

import java.awt.Dimension;
import java.util.ArrayList;

public class DimensionUtils {
	
	public static Dimension fromString(String dimension){
		String[] d = dimension.split("x");
		int width = Integer.parseInt(d[0]);
		int height = Integer.parseInt(d[1]);
		return new Dimension(width, height);
	}
	
	public static ArrayList<String> toStringArrayList(Dimension[] dimensionList){
		ArrayList<String> arrayListDimensions = new ArrayList<String>();
		for (Dimension d : dimensionList) {
			arrayListDimensions.add(new String(d.width + "x" + d.height));
		}
		return arrayListDimensions;
	}
	
	public static String toString(Dimension dimension){
		return dimension.width + "x" + dimension.height;
	}

}
