package eu.janbednar.pi4rest.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.ds.gstreamer.GStreamerDriver;

import eu.janbednar.pi4rest.utils.DimensionUtils;

@Controller
@RequestMapping("/webcam")
public class WebcamController {
	private static final Logger logger = LoggerFactory
			.getLogger(WebcamController.class);

	private static Webcam webcam;

	static {
		Webcam.setDriver(new GStreamerDriver());
		webcam = Webcam.getDefault();
	}
	/**
	 * Vraci snimek z webkamery.
	 **/
	@RequestMapping(value = "/image", method = RequestMethod.GET, produces = "image/jpg")
	@Secured(value = { "ROLE_USER" })
	public @ResponseBody
	byte[] getImage(HttpServletResponse response) throws IOException {
		logger.info("sent image");
		try {
			if (!webcam.isOpen())
				webcam.open();

			BufferedImage img = webcam.getImage();
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			ImageIO.write(img, "jpg", bao);
			return bao.toByteArray();

		} catch (IOException e) {
			logger.error(e.getMessage());
			response.sendError(500, e.getMessage());
		}
		return null;
	}
	/**
	 * Uzavre webkameru a v pripade uspechu vraci true. 
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/stop", method = RequestMethod.GET)
	public @ResponseBody
	Boolean stop() {
		if (webcam.isOpen()) {
			webcam.close();
		}
		return !webcam.isOpen();
	}
	/**
	 * Nastavuje rozliseni, prijima string ve formatu SIRKAxVYSKA
	 **/
	@Secured(value = { "ROLE_ADMIN" })
	@ResponseBody
	@RequestMapping(value = "/resolution/set", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void setResolution(@RequestBody String resString,
			HttpServletResponse response) throws IOException {
		try {
			if (webcam.isOpen()) {
				webcam.close();
			}
			webcam.setViewSize(DimensionUtils.fromString(resString));
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.sendError(400, e.getMessage());

		}

	}
	/**
	 * Vraci rozliseni ve formatu SIRKAxVYSKA
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/resolution/get", method = RequestMethod.GET)
	public @ResponseBody
	String getResolution() {
		if (webcam.isOpen()) {
			webcam.close();
		}
		try {
			return DimensionUtils.toString(webcam.getViewSize());
		} catch (Exception e) {
			return new String("unknown");
		}
	}
	/**
	 * Vraci dostupna rozliseni ve formatu SIRKAxVYSKA
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/resolutions", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<String> getAvailableResolutions() {
		return DimensionUtils.toStringArrayList(webcam.getViewSizes());
	}
	/**
	 * Vraci dostupne webkamery.
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/devices", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<HashMap<String, Object>> getAllDevices() {
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		int i = 0;
		for (Webcam w : Webcam.getWebcams()) {
			HashMap<String, Object> h = new HashMap<String, Object>();
			h.put("id", i);
			h.put("name", w.getName());
			h.put("resolutions",
					DimensionUtils.toStringArrayList(w.getViewSizes()));
			i++;
			list.add(h);
		}
		return list;
	}

}
