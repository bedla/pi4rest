package eu.janbednar.pi4rest.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import eu.janbednar.pi4rest.entity.User;
import eu.janbednar.pi4rest.service.UserManager;

/**
 * @author Jan
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);

	@Autowired
	private UserManager userManager;

	@PostConstruct
	protected void initialize() {
		userManager.initialize();
	}

	/**
	 * Vrati JSON pro pozadovanym uzivatelem podle ID.
	 */
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public @ResponseBody
	User getUserById(@PathVariable int id) {
		logger.info("_ID_ " + id);

		return userManager.findById(id);
	}

	/**
	 * Vytvori noveho uzivatele.
	 * 
	 * @throws IOException
	 */
	@Secured(value = { "ROLE_ADMIN" })
	@ResponseBody
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void createNewUser(@RequestBody User input,
			HttpServletResponse response) throws IOException {
		logger.info("_CREATE_USER_ " + input.toString());

		if (input.password().length() < 60 && input.password().length() > 7) {
			userManager.save(input);
		} else {
			response.sendError(400,
					"Password length must be between 8 and 60 characters");
		}
	}

	@Secured(value = { "ROLE_USER" })
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateUser(@RequestBody User input, HttpServletResponse response)
			throws IOException {
		logger.info("_UPDATE_USER_ " + input.toString());
		User current = userManager.getCurrentLoggedUser();
		if (current.isAdmin()) {
			userManager.update(input);
		} else if (!current.isAdmin() && input.getId() == current.getId()) {
			input.setAdmin(false);// Zabranim beznemu uzivateli zmenit si roli
			userManager.update(input);
		} else {
			response.sendError(403,
					"Only admin can update roles and other users.");
		}

	}
/** Smaze uzivatele. Metoda je POST, protoze resttemplate na klientovi neumoznuje pridat authetikacni header pri metode RequestMethod.DELETE
 * 
 * @param input
 * @param response
 * @throws IOException
 */
	@Secured(value = { "ROLE_ADMIN" })
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void deleteUser(@RequestBody User input, HttpServletResponse response)
			throws IOException {
		logger.info("_DELETE_USER_ " + input.toString());
		userManager.delete(input);
	}

	/**
	 * Vrati JSON se vsemi uzivateli.
	 */

	@Secured(value = { "ROLE_ADMIN" })
	@ResponseBody
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return userManager.findAll();
	}

	/**
	 * Who Am I: Vraci prihlaseneho uzivatele.
	 **/
	@Secured(value = { "ROLE_USER" })
	@ResponseBody
	@RequestMapping(value = "/WhoAmI", method = RequestMethod.GET)
	public User getCurrentLoggedUser() {
		return userManager.getCurrentLoggedUser();
	}

}
