package eu.janbednar.pi4rest.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pi4j.io.gpio.PinState;

import eu.janbednar.pi4rest.entity.GpioPort;
import eu.janbednar.pi4rest.service.GpioPortManager;

/**
 * @author Jan
 * 
 */
@RequestMapping("/gpio")
@Controller
public class GpioPortController {

	private static final Logger logger = LoggerFactory
			.getLogger(GpioPortController.class);
	@Autowired
	private GpioPortManager gpioPortManager;

	@PostConstruct
	protected void initGpio() {
		gpioPortManager.initialize();
	}

	/**
	 * Vraci vsechny GPIO piny.
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public @ResponseBody
	List<GpioPort> getAllGpioPorts() {
		return gpioPortManager.findAll();
	}
	/**
	 * Aktualizuje pin.
	 **/
	@Secured(value = { "ROLE_ADMIN" })
	@ResponseBody
	@RequestMapping(value = "/put", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateGpioPort(@RequestBody GpioPort gpioPort) {
		gpioPortManager.update(gpioPort);

	}
	/**
	 * Vraci pin podle ID.
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public @ResponseBody
	GpioPort getGpioPortById(@PathVariable int id) {
		logger.info("_ID_ " + id);
		return gpioPortManager.findById(id);
	}
	/**
	 * Vraci hodnotu pinu podle ID.
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/getValue/{id}", method = RequestMethod.GET)
	public @ResponseBody
	boolean getValueById(@PathVariable int id) {
		logger.info("_ID_ " + id);
		return gpioPortManager.findById(id).getValue();
	}
	/**
	 * Vraci stav pinu (DIGITAL_INPUT/DIGITAL_OUTPUT) podle ID
	 **/
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/getState/{id}", method = RequestMethod.GET)
	public @ResponseBody
	PinState getStateById(@PathVariable int id) {
		logger.info("_ID_ " + id);
		return gpioPortManager.findById(id).getState();
	}

}
