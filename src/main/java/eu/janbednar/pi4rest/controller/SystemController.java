package eu.janbednar.pi4rest.controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.janbednar.pi4rest.io.System;

@Controller
@RequestMapping("/system")
public class SystemController {

	private static final Logger logger = LoggerFactory
			.getLogger(SystemController.class);
	/**
	 * Vraci vypis systemovych informaci
	 **/
	@Secured(value = { "ROLE_USER" })
	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET)
	public System getSystemProperities(HttpServletResponse response) {
		// String err = "";
		try {
			return new System();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
