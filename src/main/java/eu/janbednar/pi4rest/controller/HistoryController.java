package eu.janbednar.pi4rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.janbednar.pi4rest.entity.GpioPort;
import eu.janbednar.pi4rest.entity.History;
import eu.janbednar.pi4rest.entity.User;
import eu.janbednar.pi4rest.service.GpioPortManager;
import eu.janbednar.pi4rest.service.HistoryManager;
import eu.janbednar.pi4rest.service.UserManager;

@RequestMapping("/history")
@Controller
public class HistoryController {

	@Autowired
	private HistoryManager historyManager;
	@Autowired
	private UserManager userManager;
	@Autowired
	private GpioPortManager gpioPortManager;

	/**
	 * Kompletni vypis historie aktualizaci pinu.
	 **/
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody
	List<History> getFullHistory() {

		return historyManager.findAll();
	}
	
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/getByUserId/{id}", method = RequestMethod.GET)
	public @ResponseBody
	List<History> getByUserId(@PathVariable int id) {
		User u = new User();
		u.setId(id);
		return historyManager.findByUser(u);
	}
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/getByGpioId/{id}", method = RequestMethod.GET)
	public @ResponseBody
	List<History> getByGpioId(@PathVariable int id) {
		GpioPort g = new GpioPort();
		g.setId(id);
		return historyManager.findByGpioPort(g);
	}
	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/myHistory", method = RequestMethod.GET)
	public @ResponseBody
	List<History> getMyHistory() {

		return historyManager.findByUser(userManager.getCurrentLoggedUser());
	}
}
