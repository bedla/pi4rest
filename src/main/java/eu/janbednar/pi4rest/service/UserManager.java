package eu.janbednar.pi4rest.service;

import java.util.List;

import eu.janbednar.pi4rest.entity.User;

public interface UserManager {
	
	public void save(User user);

	public void delete(User user);

	public void update(User user);

    public List<User> findAll();

    public User findById(int id);

    public User findByName(String name);
    
    public User getCurrentLoggedUser();
    
	public void initialize();

}
