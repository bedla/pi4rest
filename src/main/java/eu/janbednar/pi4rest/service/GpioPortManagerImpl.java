package eu.janbednar.pi4rest.service;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pi4j.io.gpio.RaspiPin;

import eu.janbednar.pi4rest.dao.GpioPortDao;
import eu.janbednar.pi4rest.dao.HistoryDao;
import eu.janbednar.pi4rest.dao.UserDao;
import eu.janbednar.pi4rest.entity.GpioPort;
import eu.janbednar.pi4rest.entity.History;

@Service
@Transactional
public class GpioPortManagerImpl implements GpioPortManager {

	@Autowired
	private GpioPortDao gpioPortDao;

	@Autowired
	private HistoryDao historyDao;

	@Autowired
	private UserDao userDao;

	@Override
	public List<GpioPort> findAll() {
		return gpioPortDao.findAll();
	}

	@Override
	public GpioPort findById(int id) {
		// pin.toggle();
		return gpioPortDao.findById(id);
	}

	@Override
	public void save(GpioPort gpioPort) {
		if (gpioPort.getId() > 17)
			throw new HibernateException("Invalid GPIO ID. ID must be between 0 and 17");
		gpioPortDao.save(gpioPort);
	}

	@Override
	public void delete(GpioPort gpioPort) {
		gpioPortDao.delete(gpioPort);
	}

	@Override
	public void update(GpioPort gpioPort) {
		gpioPortDao.update(gpioPort);
		History history = new History();
		history.setGPIOPort(gpioPort);
		history.setNewValue(gpioPort.getValue());
		history.setTime(new Timestamp(System.currentTimeMillis()));
		String name = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		history.setUser(userDao.findByName(name));
		historyDao.save(history);

	}

	@Override
	public void saveIgnore(GpioPort gpioPort) {
		gpioPortDao.saveIgnore(gpioPort);

	}

	@Override
	public void initialize() {
		
		saveIgnore(new GpioPort(0, RaspiPin.GPIO_00.getName(),
				RaspiPin.GPIO_00.getAddress(), RaspiPin.GPIO_00.getName(),
				false, true));
		saveIgnore(new GpioPort(1, RaspiPin.GPIO_01.getName(),
				RaspiPin.GPIO_01.getAddress(), RaspiPin.GPIO_01.getName(),
				false, true));
		saveIgnore(new GpioPort(2, RaspiPin.GPIO_02.getName(),
				RaspiPin.GPIO_02.getAddress(), RaspiPin.GPIO_02.getName(),
				false, true));
		saveIgnore(new GpioPort(3, RaspiPin.GPIO_03.getName(),
				RaspiPin.GPIO_03.getAddress(), RaspiPin.GPIO_03.getName(),
				false, true));
		saveIgnore(new GpioPort(4, RaspiPin.GPIO_04.getName(),
				RaspiPin.GPIO_04.getAddress(), RaspiPin.GPIO_04.getName(),
				false, true));
		saveIgnore(new GpioPort(5, RaspiPin.GPIO_05.getName(),
				RaspiPin.GPIO_05.getAddress(), RaspiPin.GPIO_05.getName(),
				false, true));
		saveIgnore(new GpioPort(6, RaspiPin.GPIO_06.getName(),
				RaspiPin.GPIO_06.getAddress(), RaspiPin.GPIO_06.getName(),
				false, true));
		saveIgnore(new GpioPort(7, RaspiPin.GPIO_07.getName(),
				RaspiPin.GPIO_07.getAddress(), RaspiPin.GPIO_07.getName(),
				false, true));
		saveIgnore(new GpioPort(8, RaspiPin.GPIO_08.getName(),
				RaspiPin.GPIO_08.getAddress(), RaspiPin.GPIO_08.getName(),
				true, true));
		saveIgnore(new GpioPort(9, RaspiPin.GPIO_09.getName(),
				RaspiPin.GPIO_09.getAddress(), RaspiPin.GPIO_09.getName(),
				true, true));
		saveIgnore(new GpioPort(10, RaspiPin.GPIO_10.getName(),
				RaspiPin.GPIO_10.getAddress(), RaspiPin.GPIO_10.getName(),
				true, true));
		saveIgnore(new GpioPort(11, RaspiPin.GPIO_11.getName(),
				RaspiPin.GPIO_11.getAddress(), RaspiPin.GPIO_11.getName(),
				true, true));
		saveIgnore(new GpioPort(12, RaspiPin.GPIO_12.getName(),
				RaspiPin.GPIO_12.getAddress(), RaspiPin.GPIO_12.getName(),
				true, true));
		saveIgnore(new GpioPort(13, RaspiPin.GPIO_13.getName(),
				RaspiPin.GPIO_13.getAddress(), RaspiPin.GPIO_13.getName(),
				true, true));
		saveIgnore(new GpioPort(14, RaspiPin.GPIO_14.getName(),
				RaspiPin.GPIO_14.getAddress(), RaspiPin.GPIO_14.getName(),
				true, true));
		saveIgnore(new GpioPort(15, RaspiPin.GPIO_15.getName(),
				RaspiPin.GPIO_15.getAddress(), RaspiPin.GPIO_15.getName(),
				true, true));
		saveIgnore(new GpioPort(16, RaspiPin.GPIO_16.getName(),
				RaspiPin.GPIO_16.getAddress(), RaspiPin.GPIO_16.getName(),
				true, true));
	}

}
