package eu.janbednar.pi4rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.janbednar.pi4rest.dao.HistoryDao;
import eu.janbednar.pi4rest.entity.GpioPort;
import eu.janbednar.pi4rest.entity.History;
import eu.janbednar.pi4rest.entity.User;

@Service
@Transactional
public class HistoryManagerImpl implements HistoryManager {

	@Autowired
	private HistoryDao historyDao;

	@Override
	public List<History> findAll() {
		return historyDao.findAll();
	}

	@Override
	public History findById(int id) {
		return historyDao.findById(id);
	}


	@Override
	public void save(History history) {
		historyDao.save(history);
	}

	@Override
	public void delete(History history) {
		historyDao.delete(history);
	}

	@Override
	public void update(History history) {
		historyDao.update(history);
	}

	@Override
	public List<History> findByUser(User u) {
	return historyDao.findByUser(u);
	}

	@Override
	public List<History> findByGpioPort(GpioPort g) {
		return historyDao.findByGpioPort(g);
	}

}
