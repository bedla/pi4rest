package eu.janbednar.pi4rest.service;

import java.util.List;

import eu.janbednar.pi4rest.entity.GpioPort;

public interface GpioPortManager {

	public void save(GpioPort gpioPort);

	public void saveIgnore(GpioPort gpioPort);

	public void delete(GpioPort gpioPort);

	public void update(GpioPort gpioPort);

	public List<GpioPort> findAll();

	public GpioPort findById(int id);

	public void initialize();

}
