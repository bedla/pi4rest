package eu.janbednar.pi4rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.janbednar.pi4rest.dao.UserDao;
import eu.janbednar.pi4rest.entity.User;

@Service
@Transactional
public class UserManagerImpl implements UserManager {

	@Autowired
	private UserDao userDao;

	@Override
	public List<User> findAll() {
		return userDao.findAll();
	}

	@Override
	public User findById(int id) {
		return userDao.findById(id);
	}

	@Override
	public User findByName(String name) {
		return userDao.findByName(name);
	}

	@Override
	public void save(User user) {
		userDao.save(hashPassword(user));
	}

	@Override
	public void delete(User user) {
		userDao.delete(user);
	}

	@Override
	public void update(User user) {
	userDao.update(hashPassword(user));
	}


	@Override
	public User getCurrentLoggedUser() {
		String name = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		return userDao.findByName(name);
	}

	private User hashPassword(User user) {
		if (user.password().length() < 60 && user.password().length()>7) {
			ShaPasswordEncoder sha = new ShaPasswordEncoder(256);
			String encodedPassword = sha.encodePassword(user.password(),
					user.getUsername());
			user.setPassword(encodedPassword);
		}
		else{
			user.setPassword(this.findById(user.getId()).password());
		}
		return user;
	}
	
	public void initialize(){
		if(userDao.count()==0){
			User user = new User();
			user.setActive(true);
			user.setAdmin(true);
			user.setPassword("password");
			user.setUsername("admin");
			save(user);
		}
		
	}
}
