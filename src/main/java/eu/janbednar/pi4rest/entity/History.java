package eu.janbednar.pi4rest.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;



/**
 * Entity implementation class for Entity: History
 * 
 */
@Entity

@Table(name = "HISTORY")
public class History implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@OneToOne
	@JoinColumn(name = "GPIOPort_id")
	private GpioPort GPIOPort;

	
	private Timestamp time;
	
	private boolean newValue;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	private static final long serialVersionUID = 1L;

	public History() {
		super();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GpioPort getGPIOPort() {
		return this.GPIOPort;
	}

	public void setGPIOPort(GpioPort GPIOPort) {
		this.GPIOPort = GPIOPort;
	}

	public Date getTime() {
		return new Date(time.getTime());
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Date time) {
		this.time = new Timestamp(time.getTime());
	}

	public boolean getNewValue() {
		return this.newValue;
	}

	public void setNewValue(boolean newValue) {
		this.newValue = newValue;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public String toString(){
		return "History [id=" + id + ", time=" + time.toString() + ", newValue=" + newValue + ", gpioPort="
	+ GPIOPort.toString()+ ", user=" + user.toString()+"]";
		
	}
}
