package eu.janbednar.pi4rest.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.pi4j.io.gpio.PinState;

import eu.janbednar.pi4rest.io.HwPin;
import eu.janbednar.pi4rest.utils.GpioUtils;

/**
 * Entity implementation class for Entity: GPPIOPort
 * 
 */
@org.hibernate.annotations.Entity(dynamicUpdate = true, selectBeforeUpdate = true)
@Entity
@Table(name = "GPIOPORTS")
public class GpioPort implements Serializable {

	/**
	 * @param isInput
	 *            the isInput to set
	 */

	@Id
	private int id;
	private String name;
	private String comment;
	private boolean isInput;
	@Transient
	private Boolean value;
	private static final long serialVersionUID = 1L;

	public GpioPort() {
	}

	public GpioPort(int id, String name, int hwAddress, String comment,
			boolean isInput, boolean value) {
		super();
		setId(id);
		setName(name);
		setComment(comment);
		setIsInput(isInput);
		setValue(value);
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean getIsInput() {
		return this.isInput;
	}

	public PinState getState() {
		return GpioUtils.booleanToPinState(value);
	}

	public void setIsInput(boolean isInput) {
		this.isInput = isInput;
	}

	@Override
	public String toString() {
		return "GPIOPort [id=" + id + ", name=" + name + ", comment=" + comment + ", isInput=" + isInput
				+ "]";
	}
	
	public boolean getValue() {
		PinState pinState;
		if (isInput) {
			pinState = new HwPin(id).getInput().getState();
			
			return GpioUtils.pinStateToBoolean(pinState);// Vraci true, pokud je na vstupu logicka 1													// 1
		}
		
		pinState =  new HwPin(id).getOutput().getState();
		return GpioUtils.pinStateToBoolean(pinState);
	}

	public void setValue(boolean value) {
		this.value = value;
		if (!isInput) {// value lze menit pouze u vystupnich portu
			PinState pinState = GpioUtils.booleanToPinState(value);
			new HwPin(id).getOutput().setState(pinState);
		}

	}
}
