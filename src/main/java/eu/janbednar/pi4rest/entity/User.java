package eu.janbednar.pi4rest.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: User
 * 
 */
/**
 * @author Jan
 *
 */
@org.hibernate.annotations.Entity(dynamicUpdate = true, selectBeforeUpdate = true)
@Entity
@Table(name="USERS")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(unique = true)
	private String username;
	private String password;
	private boolean isAdmin;
	private boolean isActive;

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public User() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


/**
 * Getter pro zasifrovane heslo. Prejmenovano z getPassword, aby nebylo serializovano do JSONu.
*/
	public String password() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public boolean isAdmin() {
		return isAdmin;
	}


	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public String getRole() {
		if(isAdmin) return "ROLE_ADMIN";
		return "ROLE_USER";
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", isAdmin=" + isAdmin +"]";
	}	


}