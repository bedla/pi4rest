package eu.janbednar.pi4rest.io;

import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiGpioProvider;
import com.pi4j.io.gpio.impl.PinImpl;

public class HwPin {
	int address;
	GpioPin oldInstance;
	final GpioController gpio = GpioFactory.getInstance();
	private static final Logger logger = LoggerFactory.getLogger(HwPin.class);

	public HwPin(int address) {
		this.address = address;

		for (GpioPin g : gpio.getProvisionedPins()) {
			if (g.getPin().getAddress() == address) {
				oldInstance = g;
				break;
			}
		}

	}

	public GpioPinDigitalInput getInput() {
		logger.info("Created input pin: " + address);	
		if(oldInstance != null)oldInstance.unexport();
		return gpio.provisionDigitalInputPin(createDigitalPin(address));

	}

	public GpioPinDigitalOutput getOutput() {
		logger.info("Created output pin: " + address);
		if (oldInstance == null){
			return gpio.provisionDigitalOutputPin(createDigitalPin(address));
		};
		
		if (oldInstance.isMode(PinMode.DIGITAL_OUTPUT)){
			PinState state = oldInstance.getProvider().getState(oldInstance.getPin());
			return gpio.provisionDigitalOutputPin(createDigitalPin(address),state);
			};
		return gpio.provisionDigitalOutputPin(createDigitalPin(address));
	}

	private static Pin createDigitalPin(int address) {
		String name = "GPIO " + address;
		return new PinImpl(RaspiGpioProvider.NAME, address, name, EnumSet.of(
				PinMode.DIGITAL_INPUT, PinMode.DIGITAL_OUTPUT),
				PinPullResistance.all());
	}

}
