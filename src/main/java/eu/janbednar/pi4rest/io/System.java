package eu.janbednar.pi4rest.io;

import java.io.IOException;
import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.pi4j.system.NetworkInfo;
import com.pi4j.system.SystemInfo;

public class System implements Serializable {
	private static final long serialVersionUID = 1L;

	private String boardType;
	private long clockFrequencyArm;
	private long clockFrequencyCore;
	private long clockFrequencyDPI;
	private long clockFrequencyEMMC;
	private long clockFrequencyH264;
	private long clockFrequencyHDMI;
	private long clockFrequencyISP;
	private long clockFrequencyPixel;
	private long clockFrequencyPWM;
	private long clockFrequencyUART;
	private long clockFrequencyV3D;
	private long clockFrequencyVEC;
	private boolean codecH264Enabled;
	private boolean codecMPG2Enabled;
	private boolean codecWVC1Enabled;
	private String cpuArchitecture;
	private String[] cpuFeatures;
	private String cpuImplementer;
	private String cpuPart;
	private String cpuRevision;
	private float cpuTemperature;
	private String cpuVariant;
	private float cpuVoltage;
	private String hardware;
	private String javaRuntime;
	private String javaVendor;
	private String javaVendorUrl;
	private String javaVersion;
	private String javaVirtualMachine;
	private long memoryBuffers;
	private long memoryCached;
	private long memoryFree;
	private long memoryShared;
	private long memoryTotal;
	private long memoryUsed;
	private float memoryVoltageSDRam_C;
	private float memoryVoltageSDRam_I;
	private float memoryVoltageSDRam_P;
	private String osArch;
	private String osFirmwareBuild;
	private String osFirmwareDate;
	private String osName;
	private String osVersion;
	private String revision;
	private String serial;
	private boolean isHardFloatAbi;

	private String hostname;

	private String[] ipAddresses;

	private String[] fqdns;

	private String[] nameservers;

	private String systemTime;

	private String upTime;

	public System() throws IOException, InterruptedException, ParseException {
		Runtime.getRuntime().gc();
		Runtime.getRuntime().gc();
		this.systemTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
				.format(new Date());

		DateFormat df = new SimpleDateFormat("dd:HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT-1"));
		this.upTime = df.format(new Date(ManagementFactory.getRuntimeMXBean()
				.getUptime()));
		this.hostname = NetworkInfo.getHostname();

		this.ipAddresses = NetworkInfo.getIPAddresses();

		this.fqdns = NetworkInfo.getFQDNs();

		this.nameservers = NetworkInfo.getNameservers();

		this.boardType = SystemInfo.getBoardType().toString();

		this.clockFrequencyArm = SystemInfo.getClockFrequencyArm();

		this.clockFrequencyCore = SystemInfo.getClockFrequencyCore();

		this.clockFrequencyDPI = SystemInfo.getClockFrequencyDPI();

		this.clockFrequencyEMMC = SystemInfo.getClockFrequencyEMMC();

		this.clockFrequencyH264 = SystemInfo.getClockFrequencyH264();

		this.clockFrequencyHDMI = SystemInfo.getClockFrequencyHDMI();

		this.clockFrequencyISP = SystemInfo.getClockFrequencyISP();

		this.clockFrequencyPixel = SystemInfo.getClockFrequencyPixel();

		this.clockFrequencyPWM = SystemInfo.getClockFrequencyPWM();

		this.clockFrequencyUART = SystemInfo.getClockFrequencyUART();

		this.clockFrequencyV3D = SystemInfo.getClockFrequencyV3D();

		this.clockFrequencyVEC = SystemInfo.getClockFrequencyVEC();

		this.codecH264Enabled = SystemInfo.getCodecH264Enabled();

		this.codecMPG2Enabled = SystemInfo.getCodecMPG2Enabled();

		this.codecWVC1Enabled = SystemInfo.getCodecWVC1Enabled();

		this.cpuArchitecture = SystemInfo.getCpuArchitecture();

		this.cpuFeatures = SystemInfo.getCpuFeatures();

		this.cpuImplementer = SystemInfo.getCpuImplementer();

		this.cpuPart = SystemInfo.getCpuPart();

		this.cpuRevision = SystemInfo.getCpuRevision();

		this.cpuTemperature = SystemInfo.getCpuTemperature();

		this.cpuVariant = SystemInfo.getCpuVariant();

		this.cpuVoltage = SystemInfo.getCpuVoltage();

		this.hardware = SystemInfo.getHardware();

		this.javaRuntime = SystemInfo.getJavaRuntime();
		this.javaVendor = SystemInfo.getJavaVendor();
		this.javaVendorUrl = SystemInfo.getJavaVendorUrl();
		this.javaVersion = SystemInfo.getJavaVersion();
		this.javaVirtualMachine = SystemInfo.getJavaVirtualMachine();

		this.memoryBuffers = SystemInfo.getMemoryBuffers();

		this.memoryCached = SystemInfo.getMemoryCached();

		this.memoryFree = SystemInfo.getMemoryFree();

		this.memoryShared = SystemInfo.getMemoryShared();

		this.memoryTotal = SystemInfo.getMemoryTotal();

		this.memoryUsed = SystemInfo.getMemoryUsed();

		this.memoryVoltageSDRam_C = SystemInfo.getMemoryVoltageSDRam_C();

		this.memoryVoltageSDRam_I = SystemInfo.getMemoryVoltageSDRam_I();

		this.memoryVoltageSDRam_P = SystemInfo.getMemoryVoltageSDRam_P();

		this.osArch = SystemInfo.getOsArch();

		this.osFirmwareBuild = SystemInfo.getOsFirmwareBuild();

		this.osFirmwareDate = SystemInfo.getOsFirmwareDate();

		this.osName = SystemInfo.getOsName();
		this.osVersion = SystemInfo.getOsVersion();

		this.revision = SystemInfo.getRevision();

		this.serial = SystemInfo.getSerial();

		this.isHardFloatAbi = SystemInfo.isHardFloatAbi();
	}

	public String getUpTime() {
		return upTime;
	}

	public String getSystemTime() {
		return systemTime;
	}

	public String getHostname() {
		return hostname;
	}

	public String[] getIpAddresses() {
		return ipAddresses;
	}

	public String[] getFqdns() {
		return fqdns;
	}

	public String[] getNameservers() {
		return nameservers;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBoardType() {
		return boardType;
	}

	public long getClockFrequencyArm() {
		return clockFrequencyArm;
	}

	public long getClockFrequencyCore() {
		return clockFrequencyCore;
	}

	public long getClockFrequencyDPI() {
		return clockFrequencyDPI;
	}

	public long getClockFrequencyEMMC() {
		return clockFrequencyEMMC;
	}

	public long getClockFrequencyH264() {
		return clockFrequencyH264;
	}

	public long getClockFrequencyHDMI() {
		return clockFrequencyHDMI;
	}

	public long getClockFrequencyISP() {
		return clockFrequencyISP;
	}

	public long getClockFrequencyPixel() {
		return clockFrequencyPixel;
	}

	public long getClockFrequencyPWM() {
		return clockFrequencyPWM;
	}

	public long getClockFrequencyUART() {
		return clockFrequencyUART;
	}

	public long getClockFrequencyV3D() {
		return clockFrequencyV3D;
	}

	public long getClockFrequencyVEC() {
		return clockFrequencyVEC;
	}

	public boolean isCodecH264Enabled() {
		return codecH264Enabled;
	}

	public boolean isCodecMPG2Enabled() {
		return codecMPG2Enabled;
	}

	public boolean isCodecWVC1Enabled() {
		return codecWVC1Enabled;
	}

	public String getCpuArchitecture() {
		return cpuArchitecture;
	}

	public String[] getCpuFeatures() {
		return cpuFeatures;
	}

	public String getCpuImplementer() {
		return cpuImplementer;
	}

	public String getCpuPart() {
		return cpuPart;
	}

	public String getCpuRevision() {
		return cpuRevision;
	}

	public float getCpuTemperature() {
		return cpuTemperature;
	}

	public String getCpuVariant() {
		return cpuVariant;
	}

	public float getCpuVoltage() {
		return cpuVoltage;
	}

	public String getHardware() {
		return hardware;
	}

	public String getJavaRuntime() {
		return javaRuntime;
	}

	public String getJavaVendor() {
		return javaVendor;
	}

	public String getJavaVendorUrl() {
		return javaVendorUrl;
	}

	public String getJavaVersion() {
		return javaVersion;
	}

	public String getJavaVirtualMachine() {
		return javaVirtualMachine;
	}

	public long getMemoryBuffers() {
		return memoryBuffers;
	}

	public long getMemoryCached() {
		return memoryCached;
	}

	public long getMemoryFree() {
		return memoryFree;
	}

	public long getMemoryShared() {
		return memoryShared;
	}

	public long getMemoryTotal() {
		return memoryTotal;
	}

	public long getMemoryUsed() {
		return memoryUsed;
	}

	public float getMemoryVoltageSDRam_C() {
		return memoryVoltageSDRam_C;
	}

	public float getMemoryVoltageSDRam_I() {
		return memoryVoltageSDRam_I;
	}

	public float getMemoryVoltageSDRam_P() {
		return memoryVoltageSDRam_P;
	}

	public String getOsArch() {
		return osArch;
	}

	public String getOsFirmwareBuild() {
		return osFirmwareBuild;
	}

	public String getOsFirmwareDate() {
		return osFirmwareDate;
	}

	public String getOsName() {
		return osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public String getRevision() {
		return revision;
	}

	public String getSerial() {
		return serial;
	}

	public boolean isHardFloatAbi() {
		return isHardFloatAbi;
	}

}