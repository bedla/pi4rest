package eu.janbednar.pi4rest.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import eu.janbednar.pi4rest.entity.GpioPort;

@Repository
public class GpioPortDaoImpl implements GpioPortDao {

	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger logger = LoggerFactory
			.getLogger(GpioPortDaoImpl.class);

	@Override
	public GpioPort findById(int id) {
		return (GpioPort) this.sessionFactory.getCurrentSession().get(
				GpioPort.class, id);
	}

	@Override
	public void save(GpioPort gpioPort) {
		this.sessionFactory.getCurrentSession().save(gpioPort);
	}

	@Override
	public void delete(GpioPort gpioPort) {
		this.sessionFactory.getCurrentSession().delete(gpioPort);
	}

	@Override
	public void update(GpioPort gpioPort) {
		this.sessionFactory.getCurrentSession().merge(gpioPort);
	}

	@Override
	public List<GpioPort> findAll() {
		return this.sessionFactory.getCurrentSession()
				.createQuery("FROM GpioPort g ORDER BY g.id ASC").list();
	}

	@Override
	public void saveIgnore(GpioPort gpioPort) {
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(gpioPort);
		} catch (ConstraintViolationException e) {
			logger.info("Ingored insert: " + gpioPort.toString());
		}
	}

}
