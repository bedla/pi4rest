package eu.janbednar.pi4rest.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import eu.janbednar.pi4rest.entity.User;


@Repository
public class UserDaoImpl implements UserDao {
	
	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
    public User findById(int id) {
        return (User) this.sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Override
    public User findByName(String username) {

        return (User) this.sessionFactory.getCurrentSession()
                .createQuery("FROM User WHERE username = :username")
                .setString("username", username).uniqueResult();
    }

	@Override
	public void save(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public void delete(User user) {
		this.sessionFactory.getCurrentSession().delete(user);
	}

	@Override
	public void update(User user) {
		this.sessionFactory.getCurrentSession().merge(user);
		
	}

	@Override
	public List<User> findAll() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM User u ORDER BY u.id ASC").list();
	}

	public int count(){
		int count = ((Long)this.sessionFactory.getCurrentSession().createQuery("select count(*) from User").uniqueResult()).intValue();
		return count;
	}

}
