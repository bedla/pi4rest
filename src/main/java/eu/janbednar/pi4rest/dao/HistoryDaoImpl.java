package eu.janbednar.pi4rest.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import eu.janbednar.pi4rest.entity.GpioPort;
import eu.janbednar.pi4rest.entity.History;
import eu.janbednar.pi4rest.entity.User;


@Repository
public class HistoryDaoImpl implements HistoryDao {
	
	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
    public History findById(int id) {
        return (History) this.sessionFactory.getCurrentSession().get(History.class, id);
    }

    @Override
    public List<History> findByUser(User user) {
        return this.sessionFactory.getCurrentSession()
                .createQuery("FROM History WHERE user_id = :user")
                .setInteger("user", user.getId()).list();
    }
    
    @Override
    public List<History> findByGpioPort(GpioPort gpioPort) {
        return this.sessionFactory.getCurrentSession()
                .createQuery("FROM History WHERE GPIOPort_id = :gpio")
                .setInteger("gpio", gpioPort.getId()).list();
    }

	@Override
	public void save(History history) {
		this.sessionFactory.getCurrentSession().save(history);
	}

	@Override
	public void delete(History history) {
		this.sessionFactory.getCurrentSession().delete(history);
	}

	@Override
	public void update(History history) {
		this.sessionFactory.getCurrentSession().update(history);
	}

	@Override
	public List<History> findAll() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM History h ORDER BY h.id ASC").list();
	}


}
