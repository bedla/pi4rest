package eu.janbednar.pi4rest.dao;

import java.util.List;

import eu.janbednar.pi4rest.entity.GpioPort;

public interface GpioPortDao {

	public void save(GpioPort gpioPort);

	public void delete(GpioPort gpioPort);

	public void update(GpioPort gpioPort);

	public List<GpioPort> findAll();

	public GpioPort findById(int id);

	public void saveIgnore(GpioPort gpioPort);

}
