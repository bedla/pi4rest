package eu.janbednar.pi4rest.dao;

import java.util.List;

import eu.janbednar.pi4rest.entity.GpioPort;
import eu.janbednar.pi4rest.entity.History;
import eu.janbednar.pi4rest.entity.User;

public interface HistoryDao {
	
	public void save(History history);

	public void delete(History history);

	public void update(History history);

    public List<History> findAll();

    public History findById(int id);
    
    public List<History> findByUser(User u);
    
    public List<History> findByGpioPort(GpioPort g);

}
